package smartbook.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class livre implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	int numSerie;
	String nom;
	String rubrique;
	//constrecteur
	public livre() {
	}
	//Getter&setters
	public int getNumSerie() {
		return numSerie;
	}
	public void setNumSerie(int numSerie) {
		this.numSerie = numSerie;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getRubrique() {
		return rubrique;
	}
	public void setRubrique(String rubrique) {
		this.rubrique = rubrique;
	}
	@Override
	public String toString() {
		return "livre [numSerie=" + numSerie + ", nom=" + nom + ", rubrique=" + rubrique + "]";
	}
	
}
