package smartbook.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import smartbook.entities.Utilisateur;

@Stateless
public class GestionUtilisateur implements GestionUtilisateurRemote, GestionUtilisateurLocal
{
    /**
     * Default constructor. 
     */
    public GestionUtilisateur(){
    }
    @PersistenceContext( unitName = "SmartBookStoreEJB" )
    private EntityManager em;

	@Override
	public void addUtilisateur(Utilisateur utilisateur) {
		em.persist(utilisateur);
		
	}

	@Override
	public void updateEtudiant(Utilisateur utilisateur) {
		em.merge(utilisateur);
		
	}

	@Override
	public Utilisateur findUtilisateurByCin(int cin) {
		return em.find(Utilisateur.class, cin);
	}

	@Override
	public void deleteEtudiant(Utilisateur utilisateur) {
		em.remove(em.merge(utilisateur));
		
	}

	@Override
	public List<Utilisateur> findAllUtilisateur() {
		Query et = em.createQuery("From Utilisateur");
		return et.getResultList();
	}

	@Override
	public Utilisateur loginUtilisateur(String email, String password) {
		try {
			Query req=em.createQuery("select u from Utilisateur u where u.email=:m AND u.password=:p");
					req.setParameter("m",email);
					req.setParameter("p",password);
					return  (Utilisateur) req.getSingleResult();
			}
			catch (Exception e)
			{
				return null;
			}
	}
    
}
