package smartbook.service;

import java.util.List;

import javax.ejb.Remote;

import smartbook.entities.Utilisateur;
@Remote
public interface GestionUtilisateurRemote {
	 void addUtilisateur(Utilisateur utilisateur) ;
	 public void updateEtudiant(Utilisateur utilisateur);
	 public Utilisateur findUtilisateurByCin(int cin);
	 public void deleteEtudiant(Utilisateur utilisateur);
	 public List<Utilisateur> findAllUtilisateur();
	 public Utilisateur loginUtilisateur(String email, String password);
}

